﻿/*
 * Created by SharpDevelop.
 * User: Zach.Read
 * Date: 18-06-15
 * Time: 13:55
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

	


namespace Mo_Information
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		string connDB = "Data Source=10.127.34.15;Initial Catalog=LabelPrintDB;User ID=fastengine;Password=Csi456";	
			
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			ShowAllProductionOrder();
		
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
	
		
	

		    public DataTable PullData(string query,string connString)
    {
		    	try{
		    		DataTable dt = new DataTable();
        			SqlConnection conn = new SqlConnection(connString);        
      				SqlCommand cmd = new SqlCommand(query, conn);
       				conn.Open();

       				 // create data adapter
        			SqlDataAdapter da = new SqlDataAdapter(cmd);
        			// this will query your database and return the result to your datatable
       			 	da.Fill(dt);
        			conn.Close();
        			da.Dispose();
        			return dt;
		    	}
		    	catch (Exception e)
		    	{
		    		throw e;								    	
		    	}
		    
		
    }
		    
		    
		    public int PutsData(string query,string connString)
    {
		    	try{
		    		SqlConnection conn = new SqlConnection(connString);
		    		conn.Open();
   					SqlCommand myCommand = new SqlCommand(query,conn);

   					int i = myCommand.ExecuteNonQuery();  
   					conn.Close();
   					return i;
		    	}
		    	catch (Exception e)
		    	{
		    		throw e;								    	
		    	}
		    
		
    }
		    
		    
		
		
		void AddToExistingClick(object sender, EventArgs e)
		{
			int RowIndex;
			if(!CheckPreConditions(out RowIndex))
				return;
			if(Verify_Exist(PoTxt.Text.ToString()))
			{
				MessageBox.Show("Please go to \"newGroup\" to modify existing PO!");
				return;
			}
			//Can insert
			string sql = @"insert into [LabelPrintDB].[dbo].[ProductionOrder_Mixing_Rule] values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}')";
			sql = string.Format(sql,PoTxt.Text.ToString(),
			        dataGridView1.Rows[RowIndex].Cells["PO_Group"].Value.ToString(),
			        dataGridView1.Rows[RowIndex].Cells["Module_Type"].Value.ToString(),
			        dataGridView1.Rows[RowIndex].Cells["Certification"].Value.ToString(),
                    dataGridView1.Rows[RowIndex].Cells["System_Voltage"].Value.ToString(),
                    dataGridView1.Rows[RowIndex].Cells["Cell_Type"].Value.ToString(),                 
                    dataGridView1.Rows[RowIndex].Cells["Glass_Type"].Value.ToString(),
                    dataGridView1.Rows[RowIndex].Cells["BackSheet_Color"].Value.ToString(), 
                    dataGridView1.Rows[RowIndex].Cells["Connector"].Value.ToString(),
                    dataGridView1.Rows[RowIndex].Cells["Cable_Length"].Value.ToString(),                 
                    dataGridView1.Rows[RowIndex].Cells["Frame_Type"].Value.ToString(),
                    dataGridView1.Rows[RowIndex].Cells["Frame_Color"].Value.ToString(),
                    dataGridView1.Rows[RowIndex].Cells["Degradation"].Value.ToString(), 
                    dataGridView1.Rows[RowIndex].Cells["BusBar"].Value.ToString(),
                    dataGridView1.Rows[RowIndex].Cells["Market"].Value.ToString(),
                    dataGridView1.Rows[RowIndex].Cells["Exception_Specification"].Value.ToString(),                    
                    dataGridView1.Rows[RowIndex].Cells["Cell_Supplier"].Value.ToString()
                    );

			
			PutsData(sql,connDB);
			MessageBox.Show("Adding done");
			dataGridView1.Rows.Clear();
			ShowAllProductionOrder();
			return;
			
		}
		void ShowAllProductionOrder()
		{
			
			string sql = @"SELECT [PO_Group]
					      ,[Module_Type]
					      ,[Certification]
					      ,[System_Voltage]
					      ,[Cell_Type]				
					      ,[Glass_Type]
					      ,[BackSheet_Color]
					      ,[Connector]
					      ,[Cable_Length]
					      ,[Frame_Type]
					      ,[Frame_Color]
					      ,[Degradation]
						  ,[BusBar]
						  ,[Market]
						  ,[Cell_Supplier]
     					  ,[Specification]     					
						 FROM [LabelPrintDB].[dbo].[ProductionOrder_Mixing_Rule] order by [PO_Group]";
			DataTable dt = PullData(sql,connDB);
			FillDataGridView(dt);
		}
		void FillDataGridView(DataTable dt)
		{
			int i = 0;
			int CNumIG = 1;
		
			foreach (DataRow row in dt.Rows)
			{
				
				bool Is_Duplicated = false;
				for(int j = 0;  j < dataGridView1.Rows.Count; j++)
				{					
					if((dataGridView1.Rows[j].Cells["PO_Group"].Value.ToString()).Equals(row[0].ToString()))
						Is_Duplicated = true;					
				}	
				if(!Is_Duplicated)
				{
					dataGridView1.Rows.Add();
					dataGridView1.Rows[i].Cells["PO_Group"].Value = Convert.ToString(row[0]);
	            	dataGridView1.Rows[i].Cells["Module_Type"].Value = Convert.ToString(row[1]);
	            	dataGridView1.Rows[i].Cells["Certification"].Value = Convert.ToString(row[2]);
	            	dataGridView1.Rows[i].Cells["System_Voltage"].Value = Convert.ToString(row[3]);
	            	dataGridView1.Rows[i].Cells["Cell_Type"].Value = Convert.ToString(row[4]);	        
	            	dataGridView1.Rows[i].Cells["Glass_Type"].Value = Convert.ToString(row[5]);
	            	dataGridView1.Rows[i].Cells["BackSheet_Color"].Value = Convert.ToString(row[6]);
	            	dataGridView1.Rows[i].Cells["Connector"].Value = Convert.ToString(row[7]);
	            	dataGridView1.Rows[i].Cells["Cable_Length"].Value = Convert.ToString(row[8]);	            	  
	            	dataGridView1.Rows[i].Cells["Frame_Type"].Value = Convert.ToString(row[9]);
	            	dataGridView1.Rows[i].Cells["Frame_Color"].Value = Convert.ToString(row[10]);
	            	dataGridView1.Rows[i].Cells["Degradation"].Value = Convert.ToString(row[11]);
	            	dataGridView1.Rows[i].Cells["BusBar"].Value = Convert.ToString(row[12]);
	            	dataGridView1.Rows[i].Cells["Market"].Value = Convert.ToString(row[13]);
	            	dataGridView1.Rows[i].Cells["Exception_Specification"].Value = Convert.ToString(row[15]);	            	
	            	dataGridView1.Rows[i].Cells["Cell_Supplier"].Value = Convert.ToString(row[14]);
					dataGridView1.Rows[i].Cells["NumberOfPos"].Value = Convert.ToString(1);
					
	            	i++;					
					CNumIG = 1;
				}
				else 
				{	
					
					CNumIG++;
					dataGridView1.Rows[i-1].Cells["NumberOfPos"].Value = Convert.ToString(CNumIG);				
					
				}
			}
		}
		private bool CheckPreConditions(out int RowIndex)
		{
			int RowCount = 0;
			RowIndex = -1;
			if(string.IsNullOrEmpty(PoTxt.Text))
			{
				MessageBox.Show("Please fill out the Production order to continue.");
				return false;				
			}
			
			foreach (DataGridViewRow row in dataGridView1.Rows)
			{  					
				if (Convert.ToBoolean(row.Cells["Check"].Value) == true)
  				{
  					RowIndex = row.Index;
					RowCount++;
  				}			
				
			}			
			if(RowCount != 1 )
			{
				MessageBox.Show("Pease check one and only one Group.");
				return false;
			}
			if(RowIndex < 0 )
				return false;
			string selection = dataGridView1.Rows[RowIndex].Cells["PO_Group"].Value.ToString();
			
			if(string.IsNullOrEmpty(selection))
			{
				MessageBox.Show("Please select the row with info.");
				return false;
			}
			string sql = @"select * from [ProductionOrder_Mixing_Rule] where productionorder = '{0}'";
			string.Format(sql,PoTxt.Text.ToString());
			DataTable dt = PullData(sql,connDB);
			if(dt.Rows.Count > 0 )
			{
				MessageBox.Show("PO Already exist");
				
				return false;
			}
				
			DialogResult dialogResult = MessageBox.Show("Want to Add to Group:"+selection+"?","Attention", MessageBoxButtons.YesNo);
			if(dialogResult == DialogResult.Yes)
			{
    			return true;
			}
			else if (dialogResult == DialogResult.No)
			{
   				MessageBox.Show("Please Make another choice.");
   				return false;
			}
			return false;
		}
		
		
		void QueryClick(object sender, EventArgs e)
		{
			if(string.IsNullOrEmpty(PoTxt.Text.ToString()))
			{
				MessageBox.Show("Please Enter Production Order!");
				return;			
			}
			string sql = @"select PO_Group from [LabelPrintDB].[dbo].[ProductionOrder_Mixing_Rule] where ProductionOrder = '{0}'";
			sql = string.Format(sql,PoTxt.Text.ToString());
			DataTable dt = PullData(sql,connDB);
			if(dt.Rows.Count > 0)
			{
				PO_show.Text = dt.Rows[0][0].ToString();
				D_PO.Enabled = true;
			}
			else 
				MessageBox.Show("This Is An New Production Order");
		}
		
		void AddToNewClick(object sender, EventArgs e)
		{
			FormNewGroup frm = new FormNewGroup();				
			frm.StartPosition = FormStartPosition.CenterScreen;
			frm.ShowDialog();	
			dataGridView1.Rows.Clear();
			ShowAllProductionOrder();			
		}
		
		
		
		void D_POClick(object sender, EventArgs e)
		{
			string sql = @"select PO_Group from [LabelPrintDB].[dbo].[ProductionOrder_Mixing_Rule] where ProductionOrder = '{0}'";
			sql = string.Format(sql,PoTxt.Text.ToString());
			DataTable dt = PullData(sql,connDB);
			if(dt.Rows.Count > 0)
			{
				PO_show.Text = dt.Rows[0][0].ToString();
			}
			
			DialogResult dialogResult = MessageBox.Show("Want to Delete from Group:"+ PO_show.Text.ToString() +"?","Attention", MessageBoxButtons.YesNo);
			if(dialogResult == DialogResult.Yes)
			{
    			sql = @"Delete from [LabelPrintDB].[dbo].[ProductionOrder_Mixing_Rule] where ProductionOrder = '{0}'";
    			sql = string.Format(sql,PoTxt.Text.ToString());
    			int i = PutsData(sql,connDB);
    			MessageBox.Show("Deleting done");
    			D_PO.Enabled = false;
    			dataGridView1.Rows.Clear();
				ShowAllProductionOrder();
			}
			else if (dialogResult == DialogResult.No)
				return;		
		}
		
		void PoTxtTextChanged(object sender, EventArgs e)
		{
			D_PO.Enabled = false;
		}

		
		
		void D_POGroupClick(object sender, EventArgs e)
		{
			int RowCount = 0;
			int RowIndex = 0;			
			foreach (DataGridViewRow row in dataGridView1.Rows)
			{  					
				if (Convert.ToBoolean(row.Cells["Check"].Value) == true)
  				{
  					RowIndex = row.Index;
					RowCount++;
  				}			
				
			}			
			if(RowCount != 1 )
			{
				MessageBox.Show("Pease check one and only one Group.");
				return;
			}
			if(RowIndex < 0 )
				return;
			
			DialogResult dialogResult = MessageBox.Show("Want to Delete The Group:"+ PO_show.Text.ToString() +"?","Attention", MessageBoxButtons.YesNo);
			if(dialogResult == DialogResult.Yes)
			{
    		string sql = @"Delete from [LabelPrintDB].[dbo].[ProductionOrder_Mixing_Rule] where PO_Group = '{0}' ";
    		sql =  string.Format(sql, dataGridView1.Rows[RowIndex].Cells["PO_Group"].Value.ToString());
			PutsData (sql,connDB);
			MessageBox.Show("Deleting Complete");
			dataGridView1.Rows.Clear();
			ShowAllProductionOrder();    			
			}
			else if (dialogResult == DialogResult.No)
				return;		
			
			
		}
		private bool Verify_Exist(string PO)
		{
			string sql =@"Select top 1 PO_Group from [ProductionOrder_Mixing_Rule] where [ProductionOrder] = '{0}'";
			sql = string.Format(sql,PO);
			DataTable dt = PullData(sql,connDB);
			if(dt.Rows.Count > 0)
			return true;
			return false;
		}
		void BtTestingClick(object sender, EventArgs e)
		{
		
		
		}

	
}
}