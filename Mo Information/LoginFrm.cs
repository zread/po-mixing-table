﻿/*
 * Created by SharpDevelop.
 * User: Zach.Read
 * Date: 18-06-15
 * Time: 14:03
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace Mo_Information
{
	/// <summary>
	/// Description of LoginFrm.
	/// </summary>
	public partial class LoginFrm : Form
	{
		public LoginFrm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			textBox1.Select();
			textBox1.Focus();
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		
		
		void LoginClick(object sender, EventArgs e)
		{
			string user,pwd;
            user = textBox1.Text.Trim();
            if (user.Length == 0)
            {
                MessageBox.Show("Username can not be empty");
                return;
            }
            
            pwd = textBox2.Text.Trim();
            if (pwd.Length == 0)
            {
                MessageBox.Show("Password can not be empty");
                return;
            }


            if (verifyUser(user, pwd))
            {
                MainForm frm = new MainForm();
				this.Hide();
				frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();
				this.Close();	
            }
            else
            {
                MessageBox.Show("Username and password is incorrect or you do not have sufficient permissions.");
            }	
		}
		private bool verifyUser(string username, string password){
			
			String sqlCon = @"Data Source=10.127.34.15;Initial Catalog=LabelPrintDB;User ID=fastengine;Password=Csi456";
			String sqlCmd = @"SELECT [UserGroup] FROM [UserLogin] WHERE [UserNM]='" + username + "' AND [UserPW]='" + password + "'";
			String result = "";
			
			using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(sqlCon))
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                try
                {
                    conn.Open();                  
                    cmd.CommandText = sqlCmd;
                    System.Data.SqlClient.SqlDataReader read = cmd.ExecuteReader();
                    if (read.Read())
                    {                    	
                    	result = (String.Format("{0}", read[0]));
                    	if(result == "9" || result == "10" || username == "user03")
                    	{
                    		return true;
                    	}
                    	else{
                    		return false;
                    	}                    	
                    }
					return false;                    
                }
                catch{
                	MessageBox.Show("Fail");
                	return false;
                }
			}
		}
		
		private void LoginFrm_KeyPress(object sender, KeyPressEventArgs e)
		{
			if(e.KeyChar == '\r')
			{
				Login.Focus();
				LoginClick(sender, e);
			}
		}
		
	}
}
