﻿/*
 * Created by SharpDevelop.
 * User: Zach.Read
 * Date: 18-06-15
 * Time: 13:55
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Windows.Forms;

namespace Mo_Information
{
	/// <summary>
	/// Class with program entry point.
	/// </summary>
	internal sealed class Program
	{
		/// <summary>
		/// Program entry point.
		/// </summary>
		[STAThread]
		private static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			LoginFrm frm = new LoginFrm();
			frm.StartPosition = FormStartPosition.CenterScreen;
			Application.Run(frm);
		}
		
	}
}
