﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 3/8/2016
 * Time: 9:09 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Mo_Information
{
	/// <summary>
	/// Description of ItemList.
	/// </summary>
	public partial class ItemList : Form
	{
		public ItemList()
		{
	
		}
			private string _Module_Type = "";     
        public string Module_Type
        {
            get { return _Module_Type; }
            set { _Module_Type = value; }
        }
        
        //
        	private string _Certification = "";     
        public string Certification
        {
            get { return _Certification; }
            set { _Certification = value; }
        }
        //
        	private string _System_Voltage = "";     
        public string System_Voltage
        {
            get { return _System_Voltage; }
            set { _System_Voltage = value; }
        }
        //
        	private string _Cell_Type = "";     
        public string Cell_Type
        {
            get { return _Cell_Type; }
            set { _Cell_Type = value; }
        }
        //
     
     
        	private string _Glass_Type = "";     
        public string Glass_Type
        {
            get { return _Glass_Type; }
            set { _Glass_Type = value; }
        }
        //
        	private string _BackSheet_Color = "";     
        public string BackSheet_Color
        {
            get { return _BackSheet_Color; }
            set { _BackSheet_Color = value; }
        }
        //
        	private string _Connector = "";     
        public string Connector
        {
            get { return _Connector; }
            set { _Connector = value; }
        }
        //
        	private string _Cable_Length = "";     
        public string Cable_Length
        {
            get { return _Cable_Length; }
            set { _Cable_Length = value; }
        }
		
       //
         	private string _Frame_Type = "";     
        public string Frame_Type
        {
            get { return _Frame_Type; }
            set { _Frame_Type = value; }
        }
        //
        	private string _Frame_Color = "";     
        public string Frame_Color
        {
            get { return _Frame_Color; }
            set { _Frame_Color = value; }
        }
        //
        	private string _Degradation = "";     
        public string Degradation
        {
            get { return _Degradation; }
            set { _Degradation = value; }
        }
        //
        	private string _BusBar = "";     
        public string BusBar
        {
            get { return _BusBar; }
            set { _BusBar = value; }
        }
        //
           	private string _Market = "";     
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }
           	private string _Specification = "";     
        public string Specification
        {
            get { return _Specification; }
            set { _Specification = value; }
        }
           	private string _Cell_Supplier = "";     
        public string Cell_Supplier
        {
            get { return _Cell_Supplier; }
            set { _Cell_Supplier = value; }
        }
        
	}
}
