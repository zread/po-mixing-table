﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 1/22/2016
 * Time: 5:12 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace Mo_Information
{
	partial class FormNewGroup
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.MCLabel = new System.Windows.Forms.Label();
			this.PoLabel = new System.Windows.Forms.Label();
			this.PoTxt = new System.Windows.Forms.TextBox();
			this.Bt_Save = new System.Windows.Forms.Button();
			this.Bt_Cancel = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.mdTp = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.BtQuery = new System.Windows.Forms.Button();
			this.CbCertification = new System.Windows.Forms.ComboBox();
			this.CbModuleType = new System.Windows.Forms.ComboBox();
			this.CbCellType = new System.Windows.Forms.ComboBox();
			this.CbGlassType = new System.Windows.Forms.ComboBox();
			this.CBSystemVol = new System.Windows.Forms.ComboBox();
			this.CbFrameType = new System.Windows.Forms.ComboBox();
			this.CbBackSheetColor = new System.Windows.Forms.ComboBox();
			this.CbConnector = new System.Windows.Forms.ComboBox();
			this.CbCableLength = new System.Windows.Forms.ComboBox();
			this.CbDegrade = new System.Windows.Forms.ComboBox();
			this.CbFrameColor = new System.Windows.Forms.ComboBox();
			this.CbBusBar = new System.Windows.Forms.ComboBox();
			this.CbMarket = new System.Windows.Forms.ComboBox();
			this.CbSpecification = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.TbGroup = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.CbCellSup = new System.Windows.Forms.ComboBox();
			this.label14 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// MCLabel
			// 
			this.MCLabel.Location = new System.Drawing.Point(367, 80);
			this.MCLabel.Name = "MCLabel";
			this.MCLabel.Size = new System.Drawing.Size(99, 23);
			this.MCLabel.TabIndex = 13;
			this.MCLabel.Text = "Label Certification:";
			// 
			// PoLabel
			// 
			this.PoLabel.Location = new System.Drawing.Point(31, 39);
			this.PoLabel.Name = "PoLabel";
			this.PoLabel.Size = new System.Drawing.Size(100, 23);
			this.PoLabel.TabIndex = 11;
			this.PoLabel.Text = "Production Order:";
			// 
			// PoTxt
			// 
			this.PoTxt.Location = new System.Drawing.Point(140, 39);
			this.PoTxt.Name = "PoTxt";
			this.PoTxt.Size = new System.Drawing.Size(161, 20);
			this.PoTxt.TabIndex = 10;
			// 
			// Bt_Save
			// 
			this.Bt_Save.Location = new System.Drawing.Point(138, 327);
			this.Bt_Save.Name = "Bt_Save";
			this.Bt_Save.Size = new System.Drawing.Size(140, 33);
			this.Bt_Save.TabIndex = 28;
			this.Bt_Save.Text = "Save";
			this.Bt_Save.UseVisualStyleBackColor = true;
			this.Bt_Save.Click += new System.EventHandler(this.Bt_SaveClick);
			// 
			// Bt_Cancel
			// 
			this.Bt_Cancel.Location = new System.Drawing.Point(436, 327);
			this.Bt_Cancel.Name = "Bt_Cancel";
			this.Bt_Cancel.Size = new System.Drawing.Size(140, 33);
			this.Bt_Cancel.TabIndex = 29;
			this.Bt_Cancel.Text = "Cancel";
			this.Bt_Cancel.UseVisualStyleBackColor = true;
			this.Bt_Cancel.Click += new System.EventHandler(this.Bt_CancelClick);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(367, 110);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 23);
			this.label2.TabIndex = 17;
			this.label2.Text = "Cell Type:";
			// 
			// mdTp
			// 
			this.mdTp.Location = new System.Drawing.Point(31, 83);
			this.mdTp.Name = "mdTp";
			this.mdTp.Size = new System.Drawing.Size(100, 23);
			this.mdTp.TabIndex = 19;
			this.mdTp.Text = "Module Type:";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(32, 141);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(100, 23);
			this.label5.TabIndex = 23;
			this.label5.Text = "Glass Type:";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(31, 111);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(100, 23);
			this.label3.TabIndex = 27;
			this.label3.Text = "System Voltage:";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(367, 140);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(100, 23);
			this.label6.TabIndex = 33;
			this.label6.Text = "Backsheet Color:";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(32, 171);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(100, 23);
			this.label7.TabIndex = 35;
			this.label7.Text = "Connector:";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(367, 173);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(100, 23);
			this.label8.TabIndex = 37;
			this.label8.Text = "Cable Length:";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(32, 201);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(100, 23);
			this.label9.TabIndex = 39;
			this.label9.Text = "Frame Type:";
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(367, 203);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(100, 23);
			this.label10.TabIndex = 41;
			this.label10.Text = "Frame Color:";
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(367, 233);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(100, 23);
			this.label11.TabIndex = 43;
			this.label11.Text = "Degradation:";
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(32, 232);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(100, 23);
			this.label12.TabIndex = 45;
			this.label12.Text = "Bus Bar:";
			// 
			// BtQuery
			// 
			this.BtQuery.Location = new System.Drawing.Point(326, 34);
			this.BtQuery.Name = "BtQuery";
			this.BtQuery.Size = new System.Drawing.Size(86, 22);
			this.BtQuery.TabIndex = 46;
			this.BtQuery.Text = "Query";
			this.BtQuery.UseVisualStyleBackColor = true;
			this.BtQuery.Click += new System.EventHandler(this.BtQueryClick);
			// 
			// CbCertification
			// 
			this.CbCertification.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbCertification.FormattingEnabled = true;
			this.CbCertification.Location = new System.Drawing.Point(472, 80);
			this.CbCertification.Name = "CbCertification";
			this.CbCertification.Size = new System.Drawing.Size(160, 21);
			this.CbCertification.TabIndex = 48;
			// 
			// CbModuleType
			// 
			this.CbModuleType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbModuleType.FormattingEnabled = true;
			this.CbModuleType.Location = new System.Drawing.Point(138, 80);
			this.CbModuleType.Name = "CbModuleType";
			this.CbModuleType.Size = new System.Drawing.Size(160, 21);
			this.CbModuleType.TabIndex = 47;
			// 
			// CbCellType
			// 
			this.CbCellType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbCellType.FormattingEnabled = true;
			this.CbCellType.Location = new System.Drawing.Point(472, 110);
			this.CbCellType.Name = "CbCellType";
			this.CbCellType.Size = new System.Drawing.Size(160, 21);
			this.CbCellType.TabIndex = 50;
			// 
			// CbGlassType
			// 
			this.CbGlassType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbGlassType.FormattingEnabled = true;
			this.CbGlassType.Location = new System.Drawing.Point(138, 140);
			this.CbGlassType.Name = "CbGlassType";
			this.CbGlassType.Size = new System.Drawing.Size(160, 21);
			this.CbGlassType.TabIndex = 52;
			// 
			// CBSystemVol
			// 
			this.CBSystemVol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CBSystemVol.FormattingEnabled = true;
			this.CBSystemVol.Location = new System.Drawing.Point(138, 110);
			this.CBSystemVol.Name = "CBSystemVol";
			this.CBSystemVol.Size = new System.Drawing.Size(160, 21);
			this.CBSystemVol.TabIndex = 49;
			// 
			// CbFrameType
			// 
			this.CbFrameType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbFrameType.FormattingEnabled = true;
			this.CbFrameType.Location = new System.Drawing.Point(138, 200);
			this.CbFrameType.Name = "CbFrameType";
			this.CbFrameType.Size = new System.Drawing.Size(160, 21);
			this.CbFrameType.TabIndex = 56;
			// 
			// CbBackSheetColor
			// 
			this.CbBackSheetColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbBackSheetColor.FormattingEnabled = true;
			this.CbBackSheetColor.Location = new System.Drawing.Point(472, 140);
			this.CbBackSheetColor.Name = "CbBackSheetColor";
			this.CbBackSheetColor.Size = new System.Drawing.Size(160, 21);
			this.CbBackSheetColor.TabIndex = 53;
			// 
			// CbConnector
			// 
			this.CbConnector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbConnector.FormattingEnabled = true;
			this.CbConnector.Location = new System.Drawing.Point(138, 170);
			this.CbConnector.Name = "CbConnector";
			this.CbConnector.Size = new System.Drawing.Size(160, 21);
			this.CbConnector.TabIndex = 54;
			// 
			// CbCableLength
			// 
			this.CbCableLength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbCableLength.FormattingEnabled = true;
			this.CbCableLength.Location = new System.Drawing.Point(472, 170);
			this.CbCableLength.Name = "CbCableLength";
			this.CbCableLength.Size = new System.Drawing.Size(160, 21);
			this.CbCableLength.TabIndex = 55;
			// 
			// CbDegrade
			// 
			this.CbDegrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbDegrade.FormattingEnabled = true;
			this.CbDegrade.Location = new System.Drawing.Point(472, 230);
			this.CbDegrade.Name = "CbDegrade";
			this.CbDegrade.Size = new System.Drawing.Size(160, 21);
			this.CbDegrade.TabIndex = 58;
			// 
			// CbFrameColor
			// 
			this.CbFrameColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbFrameColor.FormattingEnabled = true;
			this.CbFrameColor.Location = new System.Drawing.Point(472, 200);
			this.CbFrameColor.Name = "CbFrameColor";
			this.CbFrameColor.Size = new System.Drawing.Size(160, 21);
			this.CbFrameColor.TabIndex = 57;
			// 
			// CbBusBar
			// 
			this.CbBusBar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbBusBar.FormattingEnabled = true;
			this.CbBusBar.Location = new System.Drawing.Point(138, 230);
			this.CbBusBar.Name = "CbBusBar";
			this.CbBusBar.Size = new System.Drawing.Size(160, 21);
			this.CbBusBar.TabIndex = 59;
			// 
			// CbMarket
			// 
			this.CbMarket.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbMarket.FormattingEnabled = true;
			this.CbMarket.Location = new System.Drawing.Point(138, 260);
			this.CbMarket.Name = "CbMarket";
			this.CbMarket.Size = new System.Drawing.Size(160, 21);
			this.CbMarket.TabIndex = 63;
			// 
			// CbSpecification
			// 
			this.CbSpecification.Enabled = false;
			this.CbSpecification.FormattingEnabled = true;
			this.CbSpecification.Location = new System.Drawing.Point(472, 260);
			this.CbSpecification.Name = "CbSpecification";
			this.CbSpecification.Size = new System.Drawing.Size(160, 21);
			this.CbSpecification.TabIndex = 62;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(32, 263);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 23);
			this.label1.TabIndex = 61;
			this.label1.Text = "Market:";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(366, 263);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(100, 23);
			this.label4.TabIndex = 60;
			this.label4.Text = "Specification:";
			// 
			// checkBox1
			// 
			this.checkBox1.Location = new System.Drawing.Point(597, 36);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(152, 24);
			this.checkBox1.TabIndex = 64;
			this.checkBox1.Text = "Unique Cell vendor";
			this.checkBox1.UseVisualStyleBackColor = true;
			this.checkBox1.CheckedChanged += new System.EventHandler(this.CheckBox1CheckedChanged);
			// 
			// TbGroup
			// 
			this.TbGroup.Location = new System.Drawing.Point(495, 33);
			this.TbGroup.Name = "TbGroup";
			this.TbGroup.Size = new System.Drawing.Size(55, 20);
			this.TbGroup.TabIndex = 101;
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(436, 36);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(68, 23);
			this.label13.TabIndex = 66;
			this.label13.Text = "PO Group:";
			// 
			// CbCellSup
			// 
			this.CbCellSup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbCellSup.FormattingEnabled = true;
			this.CbCellSup.Location = new System.Drawing.Point(138, 291);
			this.CbCellSup.Name = "CbCellSup";
			this.CbCellSup.Size = new System.Drawing.Size(160, 21);
			this.CbCellSup.TabIndex = 64;
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(32, 294);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(100, 23);
			this.label14.TabIndex = 102;
			this.label14.Text = "Cell Supplier:";
			// 
			// FormNewGroup
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(752, 397);
			this.Controls.Add(this.CbCellSup);
			this.Controls.Add(this.label14);
			this.Controls.Add(this.TbGroup);
			this.Controls.Add(this.label13);
			this.Controls.Add(this.checkBox1);
			this.Controls.Add(this.CbMarket);
			this.Controls.Add(this.CbSpecification);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.CbBusBar);
			this.Controls.Add(this.CbFrameColor);
			this.Controls.Add(this.CbDegrade);
			this.Controls.Add(this.CbBackSheetColor);
			this.Controls.Add(this.CbConnector);
			this.Controls.Add(this.CbCableLength);
			this.Controls.Add(this.CbFrameType);
			this.Controls.Add(this.CBSystemVol);
			this.Controls.Add(this.CbGlassType);
			this.Controls.Add(this.CbCellType);
			this.Controls.Add(this.CbModuleType);
			this.Controls.Add(this.CbCertification);
			this.Controls.Add(this.BtQuery);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.Bt_Cancel);
			this.Controls.Add(this.Bt_Save);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.mdTp);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.MCLabel);
			this.Controls.Add(this.PoLabel);
			this.Controls.Add(this.PoTxt);
			this.Name = "FormNewGroup";
			this.Text = "FormNewGroup";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.ComboBox CbCellSup;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox TbGroup;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox CbSpecification;
		private System.Windows.Forms.ComboBox CbMarket;
		private System.Windows.Forms.ComboBox CbBusBar;
		private System.Windows.Forms.ComboBox CbFrameColor;
		private System.Windows.Forms.ComboBox CbDegrade;
		private System.Windows.Forms.ComboBox CbCableLength;
		private System.Windows.Forms.ComboBox CbConnector;
		private System.Windows.Forms.ComboBox CbBackSheetColor;
		private System.Windows.Forms.ComboBox CbFrameType;
		private System.Windows.Forms.ComboBox CBSystemVol;
		private System.Windows.Forms.ComboBox CbGlassType;
		private System.Windows.Forms.ComboBox CbCellType;
		private System.Windows.Forms.ComboBox CbModuleType;
		private System.Windows.Forms.ComboBox CbCertification;
		private System.Windows.Forms.Button BtQuery;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Button Bt_Cancel;
		private System.Windows.Forms.Button Bt_Save;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label mdTp;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox PoTxt;
		private System.Windows.Forms.Label PoLabel;
		private System.Windows.Forms.Label MCLabel;
	}
}
