﻿/*
 * Created by SharpDevelop.
 * User: Zach.Read
 * Date: 18-06-15
 * Time: 13:55
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace Mo_Information
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.Check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.PO_Group = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Module_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Certification = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.System_Voltage = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Cell_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Glass_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.BackSheet_Color = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Connector = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Cable_Length = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Frame_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Frame_Color = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Degradation = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.BusBar = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Market = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Cell_Supplier = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Exception_Specification = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.NumberOfPos = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.AddToExisting = new System.Windows.Forms.Button();
			this.AddToNew = new System.Windows.Forms.Button();
			this.PoTxt = new System.Windows.Forms.TextBox();
			this.PoLabel = new System.Windows.Forms.Label();
			this.Query = new System.Windows.Forms.Button();
			this.PO_show = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.D_PO = new System.Windows.Forms.Button();
			this.D_POGroup = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.Check,
			this.PO_Group,
			this.Module_Type,
			this.Certification,
			this.System_Voltage,
			this.Cell_Type,
			this.Glass_Type,
			this.BackSheet_Color,
			this.Connector,
			this.Cable_Length,
			this.Frame_Type,
			this.Frame_Color,
			this.Degradation,
			this.BusBar,
			this.Market,
			this.Cell_Supplier,
			this.Exception_Specification,
			this.NumberOfPos});
			this.dataGridView1.Location = new System.Drawing.Point(61, 115);
			this.dataGridView1.MultiSelect = false;
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.Size = new System.Drawing.Size(889, 308);
			this.dataGridView1.TabIndex = 0;
			// 
			// Check
			// 
			this.Check.HeaderText = "";
			this.Check.Name = "Check";
			this.Check.Width = 30;
			// 
			// PO_Group
			// 
			this.PO_Group.HeaderText = "PO_Group";
			this.PO_Group.Name = "PO_Group";
			this.PO_Group.ReadOnly = true;
			// 
			// Module_Type
			// 
			this.Module_Type.HeaderText = "Module_Type";
			this.Module_Type.Name = "Module_Type";
			this.Module_Type.ReadOnly = true;
			// 
			// Certification
			// 
			this.Certification.HeaderText = "Certification";
			this.Certification.Name = "Certification";
			// 
			// System_Voltage
			// 
			this.System_Voltage.HeaderText = "System_Voltage";
			this.System_Voltage.Name = "System_Voltage";
			this.System_Voltage.ReadOnly = true;
			this.System_Voltage.Width = 200;
			// 
			// Cell_Type
			// 
			this.Cell_Type.HeaderText = "Cell_Type";
			this.Cell_Type.Name = "Cell_Type";
			this.Cell_Type.ReadOnly = true;
			// 
			// Glass_Type
			// 
			this.Glass_Type.HeaderText = "Glass_Type";
			this.Glass_Type.Name = "Glass_Type";
			this.Glass_Type.ReadOnly = true;
			// 
			// BackSheet_Color
			// 
			this.BackSheet_Color.HeaderText = "BackSheet_Color";
			this.BackSheet_Color.Name = "BackSheet_Color";
			// 
			// Connector
			// 
			this.Connector.HeaderText = "Connector";
			this.Connector.Name = "Connector";
			// 
			// Cable_Length
			// 
			this.Cable_Length.HeaderText = "Cable_Length";
			this.Cable_Length.Name = "Cable_Length";
			// 
			// Frame_Type
			// 
			this.Frame_Type.HeaderText = "Frame_Type";
			this.Frame_Type.Name = "Frame_Type";
			// 
			// Frame_Color
			// 
			this.Frame_Color.HeaderText = "Frame_Color";
			this.Frame_Color.Name = "Frame_Color";
			// 
			// Degradation
			// 
			this.Degradation.HeaderText = "Degradation";
			this.Degradation.Name = "Degradation";
			this.Degradation.ReadOnly = true;
			// 
			// BusBar
			// 
			this.BusBar.HeaderText = "BusBar";
			this.BusBar.Name = "BusBar";
			// 
			// Market
			// 
			this.Market.HeaderText = "Market";
			this.Market.Name = "Market";
			// 
			// Cell_Supplier
			// 
			this.Cell_Supplier.HeaderText = "Cell_Supplier";
			this.Cell_Supplier.Name = "Cell_Supplier";
			// 
			// Exception_Specification
			// 
			this.Exception_Specification.HeaderText = "Cell_Specification";
			this.Exception_Specification.Name = "Exception_Specification";
			// 
			// NumberOfPos
			// 
			this.NumberOfPos.HeaderText = "QTY";
			this.NumberOfPos.Name = "NumberOfPos";
			// 
			// AddToExisting
			// 
			this.AddToExisting.Location = new System.Drawing.Point(274, 453);
			this.AddToExisting.Name = "AddToExisting";
			this.AddToExisting.Size = new System.Drawing.Size(135, 35);
			this.AddToExisting.TabIndex = 1;
			this.AddToExisting.Text = "AddToExisting";
			this.AddToExisting.UseVisualStyleBackColor = true;
			this.AddToExisting.Click += new System.EventHandler(this.AddToExistingClick);
			// 
			// AddToNew
			// 
			this.AddToNew.Location = new System.Drawing.Point(491, 453);
			this.AddToNew.Name = "AddToNew";
			this.AddToNew.Size = new System.Drawing.Size(135, 35);
			this.AddToNew.TabIndex = 2;
			this.AddToNew.Text = "NewGroup";
			this.AddToNew.UseVisualStyleBackColor = true;
			this.AddToNew.Click += new System.EventHandler(this.AddToNewClick);
			// 
			// PoTxt
			// 
			this.PoTxt.Location = new System.Drawing.Point(147, 34);
			this.PoTxt.Name = "PoTxt";
			this.PoTxt.Size = new System.Drawing.Size(161, 20);
			this.PoTxt.TabIndex = 3;
			this.PoTxt.TextChanged += new System.EventHandler(this.PoTxtTextChanged);
			// 
			// PoLabel
			// 
			this.PoLabel.Location = new System.Drawing.Point(38, 34);
			this.PoLabel.Name = "PoLabel";
			this.PoLabel.Size = new System.Drawing.Size(100, 23);
			this.PoLabel.TabIndex = 4;
			this.PoLabel.Text = "Production Order:";
			// 
			// Query
			// 
			this.Query.Location = new System.Drawing.Point(61, 453);
			this.Query.Name = "Query";
			this.Query.Size = new System.Drawing.Size(135, 35);
			this.Query.TabIndex = 7;
			this.Query.Text = "Query";
			this.Query.UseVisualStyleBackColor = true;
			this.Query.Click += new System.EventHandler(this.QueryClick);
			// 
			// PO_show
			// 
			this.PO_show.Location = new System.Drawing.Point(147, 60);
			this.PO_show.Name = "PO_show";
			this.PO_show.ReadOnly = true;
			this.PO_show.Size = new System.Drawing.Size(161, 20);
			this.PO_show.TabIndex = 8;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(38, 60);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 23);
			this.label1.TabIndex = 9;
			this.label1.Text = "PO_Group:";
			// 
			// D_PO
			// 
			this.D_PO.Enabled = false;
			this.D_PO.Location = new System.Drawing.Point(274, 517);
			this.D_PO.Name = "D_PO";
			this.D_PO.Size = new System.Drawing.Size(135, 35);
			this.D_PO.TabIndex = 10;
			this.D_PO.Text = "DeletePO";
			this.D_PO.UseVisualStyleBackColor = true;
			this.D_PO.Click += new System.EventHandler(this.D_POClick);
			// 
			// D_POGroup
			// 
			this.D_POGroup.Location = new System.Drawing.Point(491, 517);
			this.D_POGroup.Name = "D_POGroup";
			this.D_POGroup.Size = new System.Drawing.Size(135, 35);
			this.D_POGroup.TabIndex = 11;
			this.D_POGroup.Text = "DeletePOGroup";
			this.D_POGroup.UseVisualStyleBackColor = true;
			this.D_POGroup.Click += new System.EventHandler(this.D_POGroupClick);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(984, 581);
			this.Controls.Add(this.D_POGroup);
			this.Controls.Add(this.D_PO);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.PO_show);
			this.Controls.Add(this.Query);
			this.Controls.Add(this.PoLabel);
			this.Controls.Add(this.PoTxt);
			this.Controls.Add(this.AddToNew);
			this.Controls.Add(this.AddToExisting);
			this.Controls.Add(this.dataGridView1);
			this.Name = "MainForm";
			this.Text = "Mo Information";
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.DataGridViewTextBoxColumn Exception_Specification;
		private System.Windows.Forms.DataGridViewTextBoxColumn Market;
		private System.Windows.Forms.DataGridViewTextBoxColumn Cell_Supplier;
		private System.Windows.Forms.DataGridViewTextBoxColumn BusBar;
		private System.Windows.Forms.DataGridViewTextBoxColumn Frame_Color;
		private System.Windows.Forms.DataGridViewTextBoxColumn Frame_Type;
		private System.Windows.Forms.DataGridViewTextBoxColumn Cable_Length;
		private System.Windows.Forms.DataGridViewTextBoxColumn Connector;
		private System.Windows.Forms.DataGridViewTextBoxColumn BackSheet_Color;
		private System.Windows.Forms.DataGridViewTextBoxColumn Certification;
		private System.Windows.Forms.DataGridViewTextBoxColumn NumberOfPos;
		private System.Windows.Forms.Button D_POGroup;
		private System.Windows.Forms.Button D_PO;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox PO_show;
		private System.Windows.Forms.Button Query;
		private System.Windows.Forms.DataGridViewCheckBoxColumn Check;
		private System.Windows.Forms.Label PoLabel;
		private System.Windows.Forms.TextBox PoTxt;
		private System.Windows.Forms.Button AddToNew;
		private System.Windows.Forms.Button AddToExisting;
		private System.Windows.Forms.DataGridViewTextBoxColumn System_Voltage;
		private System.Windows.Forms.DataGridViewTextBoxColumn Degradation;
		private System.Windows.Forms.DataGridViewTextBoxColumn Glass_Type;
		private System.Windows.Forms.DataGridViewTextBoxColumn Cell_Type;
		private System.Windows.Forms.DataGridViewTextBoxColumn Module_Type;
		private System.Windows.Forms.DataGridViewTextBoxColumn PO_Group;
		private System.Windows.Forms.DataGridView dataGridView1;
	}
}
