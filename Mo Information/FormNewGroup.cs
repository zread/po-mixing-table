﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 1/22/2016
 * Time: 5:12 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace Mo_Information
{
	/// <summary>
	/// Description of FormNewGroup.
	/// </summary>
	public partial class FormNewGroup : Form
	{
		string connDB = "Data Source=10.127.34.15;Initial Catalog=LabelPrintDB;User ID=fastengine;Password=Csi456";	
		string csisapmesconnection = "Data Source=10.127.34.15;Initial Catalog= csimes_SapInterface;User ID=fastengine;Password=Csi456";
		private const int Group_Length = 5;
		public FormNewGroup()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		
		}
		
		
		void Bt_SaveClick(object sender, EventArgs e)
		{
			if(!Verify_Inputs())
			{
				MessageBox.Show("Please Complete!");
				return;
			}
			#region Assign Value
			ItemList T = new ItemList();
			T.Module_Type = CbModuleType.Text.ToString();
			T.Certification = CbCertification.Text.ToString();
			T.Cell_Type = CbCellType.Text.ToString();
			T.System_Voltage = CBSystemVol.Text.ToString();
			T.Glass_Type = CbGlassType.Text.ToString();
			T.BackSheet_Color = CbBackSheetColor.Text.ToString();
			T.Connector = CbConnector.Text.ToString();
			T.Cable_Length = CbCableLength.Text.ToString();
			T.Frame_Type = CbFrameType.Text.ToString();
			T.Frame_Color = CbFrameColor.Text.ToString();
			T.Degradation = CbDegrade.Text.ToString();
			T.BusBar = CbBusBar.Text.ToString();
			T.Market = CbMarket.Text.ToString();			
			T.Cell_Supplier = CbCellSup.Text.ToString();
			T.Specification = (!checkBox1.Checked)?"":CbSpecification.Text.ToString();
			string PO_Group = "";
			string Po = PoTxt.Text.ToString().ToUpper();
			DataTable dt = new DataTable();
			#endregion
			if(Verify_Exist(Po))
			{
				DialogResult dialogResult = MessageBox.Show("Sure to update PO", "Warning", MessageBoxButtons.YesNo);
						if(dialogResult == DialogResult.Yes)
						{	
							string sql = "";
							#region Not In Use
//							if(!checkBox1.Checked)
//							{
//								if(string.IsNullOrEmpty( CbSpecification.Text.ToString()))
//								 	return;
//								
//								sql = @"Select top 1 PO_Group from [ProductionOrder_Mixing_Rule] where [Specification] = '"+ T.Specification +"'";
//								 dt = NewPullData(sql,connDB);
//								 if(dt.Rows.Count>0)
//								 {
//								 	PO_Group = dt.Rows[0][0].ToString();
//								 	update_Production(T,Po,PO_Group);
//								 	ClearAll();
//								 	return;
//								 }
//								else
//								{
//									sql = @" select PO_Group from ProductionOrder_Mixing_Rule where PO_Group = (select PO_Group from ProductionOrder_Mixing_Rule where ProductionOrder = '{0}')";
//									sql = string.Format(sql,Po);
//									dt = NewPullData(sql,connDB);
//									if (dt.Rows.Count == 1)
//									{
//										PO_Group = dt.Rows[0][0].ToString();
//									}									
//									else if(dt.Rows.Count > 0 && dt.Rows.Count != 1)
//									{
//										sql = @" select top 1 PO_Group from ProductionOrder_Mixing_Rule order by PO_Group desc";
//										dt = NewPullData(sql,connDB);
//										char temp = Convert.ToChar(dt.Rows[0][0]);
//											temp ++;
//											PO_Group = temp.ToString();
//									}
//									else
//									{
//										PO_Group = "A";	
//									}
//									update_Production(T,Po,PO_Group);
//									ClearAll();
//									return;
//								}
//							}
							#endregion
							
								//Changed
								sql =@"Select top 1 PO_Group from [ProductionOrder_Mixing_Rule] where [Module_Type] = '{0}'
								and [Certification] = '{1}' 
								and [System_Voltage] = '{2}'
								and [Cell_Type] = '{3}' 
								and [BackSheet_Color] = '{4}' 
								and [Connector] = '{5}'
								and [Cable_Length] = '{6}' 
								and [Frame_Type] = '{7}'
								and [Frame_Color]= '{8}' 
								and [Degradation] = '{9}'
								and [BusBar] = '{10}'
								and [Glass_Type] = '{11}'
								and [Market] = '{12}'
								and [Specification] ='{13}'";
								
								sql = string.Format(sql,T.Module_Type,T.Certification,T.System_Voltage,T.Cell_Type,T.BackSheet_Color,T.Connector,T.Cable_Length,T.Frame_Type,T.Frame_Color,T.Degradation,T.BusBar,T.Glass_Type,T.Market,T.Specification);
								
								dt = NewPullData(sql,connDB);
								if(dt.Rows.Count > 0)
								{
									PO_Group = dt.Rows[0][0].ToString();
								
									update_Production(T,Po,PO_Group);
									ClearAll();
									return;											
											
								}
								else
								{
									//CHange
									sql = @"select PO_Group from ProductionOrder_Mixing_Rule where PO_Group = (select PO_Group from ProductionOrder_Mixing_Rule where ProductionOrder = '{0}')";
									sql = string.Format(sql,Po);
									dt = NewPullData(sql,connDB);
									if (dt.Rows.Count == 1)
									{
										PO_Group = dt.Rows[0][0].ToString();
									}									
									else if(dt.Rows.Count > 0 && dt.Rows.Count != 1)
									{
										//Change
										sql = @" select top 1 PO_Group from ProductionOrder_Mixing_Rule order by PO_Group desc";
										dt = NewPullData(sql,connDB);
										int temp = Convert.ToInt16(dt.Rows[0][0]);
											temp ++;
											PO_Group = temp.ToString().PadLeft(Group_Length, '0');
										
//										char temp = Convert.ToChar(dt.Rows[0][0]);
//											temp ++;
//											PO_Group = temp.ToString();
									}
									else
									{
										PO_Group = "00001";	
									}
									update_Production(T,Po,PO_Group);
									ClearAll();
									return;
								}
						}									
						else if (dialogResult == DialogResult.No)
						{
						    return;
						}
			}							
			else
			{				
				string sql = "";
				#region Not In Use
//				if(checkBox1.Checked)
//				{
//					if(string.IsNullOrEmpty( CbSpecification.Text.ToString()))
//								 	return;
//					
//					sql = @"Select top 1 PO_Group from [ProductionOrder_Mixing_Rule] where [Specification] = '"+ T.Specification +"'";
//					dt = NewPullData(sql,connDB);
//					if(dt.Rows.Count>0)
//					{
//						PO_Group = dt.Rows[0][0].ToString();
//							InsertIntoProduction(T,Po,PO_Group);
//							ClearAll();
//							return;
//					}
//					else
//					{
//						sql = @" select top 1 PO_Group from ProductionOrder_Mixing_Rule order by PO_Group desc";
//									dt = NewPullData(sql,connDB);
//									if(dt.Rows.Count > 0 )
//									{
//										
//										 char temp = Convert.ToChar(dt.Rows[0][0]);
//											temp ++;
//											PO_Group = temp.ToString();
//									}
//									else
//									{
//										PO_Group = "A";	
//									}
//									InsertIntoProduction(T,Po,PO_Group);
//									ClearAll();
//									return;
//					}
					
//				}
			#endregion
				//Change			
				 sql =@"Select top 1 PO_Group from [ProductionOrder_Mixing_Rule] where [Module_Type] = '{0}'
								and [Certification] = '{1}' 
								and [System_Voltage] = '{2}'
								and [Cell_Type] = '{3}' 
								and [BackSheet_Color] = '{4}' 
								and [Connector] = '{5}'
								and [Cable_Length] = '{6}' 
								and [Frame_Type] = '{7}'
								and [Frame_Color]= '{8}' 
								and [Degradation] = '{9}'
								and [BusBar] = '{10}'
								and [Glass_Type] = '{11}'
								and [Market] = '{12}'
								and [Specification] = '{13}'";
								
				 sql = string.Format(sql,T.Module_Type,T.Certification,T.System_Voltage,T.Cell_Type,T.BackSheet_Color,T.Connector,T.Cable_Length,T.Frame_Type,T.Frame_Color,T.Degradation,T.BusBar,T.Glass_Type,T.Market,T.Specification);
								
								dt = NewPullData(sql,connDB);
								if(dt.Rows.Count > 0)
								{
									PO_Group = dt.Rows[0][0].ToString();
									InsertIntoProduction(T,Po,PO_Group);
									ClearAll();
								}
								else
								{
									//Change
									sql = @" select top 1 PO_Group from ProductionOrder_Mixing_Rule order by PO_Group desc";
									dt = NewPullData(sql,connDB);
									if(dt.Rows.Count > 0 )
									{
										
//										 char temp = Convert.ToChar(dt.Rows[0][0]);
//											temp ++;
//											PO_Group = temp.ToString();
										int temp = Convert.ToInt16(dt.Rows[0][0]);
											temp ++;
											PO_Group = temp.ToString().PadLeft(Group_Length, '0');
									}
									else
									{
										PO_Group = "00001";	
									}
									
									InsertIntoProduction(T,Po,PO_Group);
									ClearAll();
								}
							
			}		
						
		}
		
   
			
		
		//Change
		private void update_Production(ItemList T, string PO,string PO_Group)
		{
			string sql = @" update ProductionOrder_Mixing_Rule set [Module_Type] = '{0}' 
			, [Certification] = '{1}' 
			, [System_Voltage] = '{2}'
			, [Cell_Type] = '{3}' 
			, [BackSheet_Color] = '{4}' 
			, [Connector] = '{5}'
			, [Cable_Length] = '{6}' 
			, [Frame_Type] = '{7}'
			, [Frame_Color]= '{8}' 
			, [Degradation] = '{9}'
			, [BusBar] = '{10}'
			, [PO_Group] = '{11}'
			, [Glass_Type] = '{12}'
			, [Market] = '{14}'
			, [Specification] = '{15}'			
			, [Cell_Supplier] = '{16}'
			where [ProductionOrder] = '{13}'";
			sql = string.Format(sql,T.Module_Type,T.Certification,T.System_Voltage,T.Cell_Type,T.BackSheet_Color,T.Connector,T.Cable_Length,T.Frame_Type,T.Frame_Color,T.Degradation,T.BusBar,PO_Group,T.Glass_Type,PO,T.Market,T.Specification,T.Cell_Supplier);
			
			NewPutsData(sql,connDB);
			if(checkBox1.Checked)
			{
				sql = @"select * from materialtype where itemID = 13 and type = '{0}' ";
				sql = string.Format(sql,T.Specification);
				DataTable dt = NewPullData(sql,connDB);
				if(dt.Rows.Count > 0)
					return;
							
				sql = @"insert into MaterialType values(13,'Specification','{0}')";
				sql = string.Format(sql,T.Specification);
				NewPutsData(sql,connDB);
			}
			return;			
			
		}
		private bool Verify_Exist(string PO)
		{
			string sql =@"Select top 1 PO_Group from [ProductionOrder_Mixing_Rule] where [ProductionOrder] = '{0}'";
			sql = string.Format(sql,PO);
			DataTable dt = NewPullData(sql,connDB);
			if(dt.Rows.Count > 0)
			return true;
			return false;
		}
		private void InsertIntoProduction(ItemList T,string PO, string PO_Group)
		{
			//Change
			string sql = @" insert into ProductionOrder_Mixing_Rule values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}')";
			sql = string.Format(sql,PO,PO_Group,T.Module_Type,T.Certification,T.System_Voltage,T.Cell_Type,T.Glass_Type,T.BackSheet_Color,T.Connector,T.Cable_Length,T.Frame_Type,T.Frame_Color,T.Degradation,T.BusBar,T.Market,T.Specification,T.Cell_Supplier);
			NewPutsData(sql,connDB);
			
				if(checkBox1.Checked)
			{
				sql = @"select * from materialtype where itemID = 13 and type = '{0}' ";
				sql = string.Format(sql,T.Specification);
				DataTable dt = NewPullData(sql,connDB);
				if(dt.Rows.Count > 0)
					return;
							
				sql = @"insert into MaterialType values(13,'Specification','{0}')";
				sql = string.Format(sql,T.Specification);
				NewPutsData(sql,connDB);
			}
			return;			
		}
		private bool Verify_Inputs()
		{
			bool Good = true;
			string SP = CbSpecification.Text.ToString();
			CbSpecification.Text = "temp";
			foreach (Control ctl in this.Controls) {
				if (ctl.TabIndex >100) {
					continue;
				}
				if(string.IsNullOrEmpty(ctl.Text.ToString()))
					Good = false;
			}
			CbSpecification.Text = SP;
			return Good;
		
		}
		
		
		public DataTable NewPullData(string query,string connString)
    {
		    	try{
		    		DataTable dt = new DataTable();
        			SqlConnection conn = new SqlConnection(connString);        
      				SqlCommand cmd = new SqlCommand(query, conn);
       				conn.Open();

       				 // create data adapter
        			SqlDataAdapter da = new SqlDataAdapter(cmd);
        			// this will query your database and return the result to your datatable
       			 	da.Fill(dt);
        			conn.Close();
        			da.Dispose();
        			return dt;
		    	}
		    	catch (Exception e)
		    	{
		    		throw e;								    	
		    	}
	}
		public int NewPutsData(string query,string connString)
	    {
			    	try{
			    		SqlConnection conn = new SqlConnection(connString);
			    		conn.Open();
	   					SqlCommand myCommand = new SqlCommand(query,conn);
	
	   					int i = myCommand.ExecuteNonQuery();  
	   					MessageBox.Show("Added successful!");
	   					conn.Close();
	   					return i;
			    	}
			    	catch (Exception e)
			    	{
			    		throw e;								    	
			    	}
			    
			 
	    }	
		void ClearAll()
		{
			ClearCombobox(CbModuleType);
			ClearCombobox(CbCertification);
			ClearCombobox(CBSystemVol);
			ClearCombobox(CbCellType);
			ClearCombobox(CbGlassType);
			ClearCombobox(CbBackSheetColor);
			ClearCombobox(CbConnector);
			ClearCombobox(CbCableLength);
			ClearCombobox(CbDegrade);
			ClearCombobox(CbFrameType);
			ClearCombobox(CbFrameColor);		
			ClearCombobox(CbBusBar);
			ClearCombobox(CbMarket);
			ClearCombobox(CbSpecification);			
			ClearCombobox(CbCellSup);
			
		}
		void ClearCombobox(ComboBox box)
		{
			box.Items.Clear();
			box.Text = "";
			
		}
		
		void Bt_CancelClick(object sender, EventArgs e)
		{
			this.Close();
		}
		
		void BtQueryClick(object sender, EventArgs e)
		{
			ClearAll();
			LoadParameters();
			if(string.IsNullOrEmpty(PoTxt.Text.ToString()))
				return;				
			string PO  = PoTxt.Text.ToString();	
			if(PO.Length != 9)
				return;		
			
			string sql = @"select * from [ProductionOrder_Mixing_Rule] where ProductionOrder = '{0}'";
			sql = string.Format(sql,PO);
			DataTable dt = NewPullData(sql,connDB);
			if(dt.Rows.Count >0)
			{
				TbGroup.Text = dt.Rows[0][2].ToString();
				CbModuleType.Text = dt.Rows[0][3].ToString();
				CbCertification.Text = dt.Rows[0][4].ToString();
			
				CBSystemVol.Text = dt.Rows[0][5].ToString();
				CbCellType.Text = dt.Rows[0][6].ToString();
				CbGlassType.Text = dt.Rows[0][7].ToString();
				CbBackSheetColor.Text = dt.Rows[0][8].ToString();
				CbConnector.Text = dt.Rows[0][9].ToString();
				CbCableLength.Text = dt.Rows[0][10].ToString();
				CbFrameType.Text = dt.Rows[0][11].ToString();
				CbFrameColor.Text = dt.Rows[0][12].ToString();
				CbDegrade.Text = dt.Rows[0][13].ToString();
				CbBusBar.Text = dt.Rows[0][14].ToString();
				CbMarket.Text = dt.Rows[0][15].ToString();
				CbSpecification.Text = dt.Rows[0][16].ToString();				
				CbCellSup.Text = dt.Rows[0][17].ToString();
				
			}
			else
			{
				string YY = DateTime.Now.Year.ToString().Substring(2,2);
				string MM = DateTime.Now.ToString("MM");
		
				
				sql = @" Select top 1 ModelType,CellType from Product where SN like '1"+YY+ MM +"__"+ PO.Substring(PO.Length-3,3)+"____' ";
				dt = NewPullData(sql,connDB);
					if(dt.Rows.Count>0)
				{
					CbModuleType.Text = (dt.Rows[0][0].ToString().Equals("CS6X-P"))?"6X":"6P";
					CbCellType.Text = dt.Rows[0][1].ToString();					
				}				
				sql = "select top 1 Resv04, resv03,resv02,resv01 from TSAP_WORKORDER_SALES_REQUEST where OrderNO = '"+ PO +"' order by CreatedOn desc";
				dt = NewPullData(sql,csisapmesconnection);
				if(dt.Rows.Count>0)
				{
					CbConnector.Text = dt.Rows[0][0].ToString();
					CbCableLength.Text = dt.Rows[0][1].ToString();
					CbDegrade.Text = dt.Rows[0][2].ToString();
					CbMarket.Text = dt.Rows[0][3].ToString();
				}
				
			}
			
			
		}
		
		private void LoadParameters()
		{
			
			loadComboBox(CbModuleType,1);
			loadComboBox(CbCertification,2);
			loadComboBox(CBSystemVol,3);
			loadComboBox(CbCellType,4);
			loadComboBox(CbGlassType,5);
			loadComboBox(CbBackSheetColor,6);
			loadComboBox(CbConnector,7);
			loadComboBox(CbCableLength,8);
			loadComboBox(CbMarket,9);
			loadComboBox(CbCellSup,15);
			loadComboBox(CbDegrade,10);
			loadComboBox(CbFrameType,11);
			loadComboBox(CbFrameColor,12);
			loadComboBoxSpecification(CbSpecification);
			loadComboBox(CbBusBar,14);
			
						
		}
		
		void loadComboBoxSpecification(ComboBox box)
		{
			string sql = @" select distinct specification from ProductionOrder_Mixing_Rule ";				
				DataTable dt = NewPullData(sql,connDB);
				foreach (DataRow row in dt.Rows) {
					box.Items.AddRange(new object [] {row[0].ToString()});
					
				}
			 
		}
		void loadComboBox(ComboBox box, int index)
		{
					 
			
				string sql = @"select Type From MaterialType where ItemID = {0}";
				sql = string.Format(sql,index);
				DataTable dt = NewPullData(sql,connDB);
				foreach (DataRow row in dt.Rows) {
					box.Items.AddRange(new object [] {row[0].ToString()});
					
				}
			 
		
		
		}
		
		void CheckBox1CheckedChanged(object sender, EventArgs e)
		{
			if(checkBox1.Checked)
			{
				CbSpecification.Enabled = true;
				
			}
			else
			{
				CbSpecification.Enabled = false;
			}
		}
    }
	
}
