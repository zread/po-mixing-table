Program Created: 
April 2016

Created By;
Jacky Li

Program Used By: 
Planning

Program's Purpose: 
This program allows the planning team to enter in information about every MO they release. We cannot grab all this data from SAP so we need planning to input this information to determine which materials can be packed together and what needs to be on the carton label.

Deployed:
David Zhao and Sally Lin computers(Planning group)

Referneces:
SAP Packaging System


